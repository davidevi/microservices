SHELL:=/bin/bash
.ONESHELL :

alpha_requirements:
	pushd alpha
	poetry export -f requirements.txt > requirements.txt
	popd

beta_requirements:
	pushd beta
	poetry export -f requirements.txt > requirements.txt
	popd

# Locally deploys the microservices via docker compose
compose: alpha_requirements beta_requirements
	pushd deployment
	docker-compose up --force-recreate --always-recreate-deps --build -d
	popd

# Runs integration tests on the locally deployed docker compose stack
integration: compose
	pushd tests
	poetry install
	poetry run invoke integration
	popd
