# Microservices System Architecture
![](Microservices.png)

Given the Kubernetes and Helm based deployment approach, this solution is
cloud agnostic and only requires a Kubernetes cluster with Helm/Tiller
to be installed.

**Note:** The diagram above indicates an ingress but this is not present in the
final solution due to the necessity for a real Kubernetes cluster (not Minikube)
and more setup related to ingress controllers.
