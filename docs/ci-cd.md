# CI/CD Integration

The following changes would be made:
- Each top-level folder of this project (apart from `docs`) would end up being its own Git repository
- Each microservice would have the same CI/CD pipeline, as described below

Microservice CI/CD pipeline stages:
- Check Stage - Checks for linting and formatting
- Unit Test Stage - Runs unit tests
- Docker Build Stage - Builds docker images
- Integration Test Stage - Runs integration tests
    - Integration tests would be pulled from their own repository
    - Deployment scripts would be pulled from their own repository (only docker compose would be used here)
    - When there are dependent services, the latest stable version of those services would be used
- Mark as stable
- [Manual Step] Release
    - Allows user to release a major, minor, or patch version (i.e. Increment a number)
    - Once released, a new git tag is created and pushed
    - Once released, docker container is re-tagged with latest tag version
- Automatic Deployment with Kubernetes and Helm
    - The Helm chart would normally be in its own repository
    - This would only require a `helm upgrade` command
    - Due to the nature of Kubernetes Deployments and Helm charts, a rolling update
    would be performed where traffic would slowly be shifted from the old version pods to the new version pods
    - The upgrade would only require a change in `values.yaml` to point to the newest image
    - If the chart itself would be changed, it would be done so in its own repository and tested separately

