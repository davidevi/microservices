# Microservices Deployment Scripts


## Quick and Easy Local Deployment
Requires Docker Compose (Tested on `1.25.4`)
```
docker-compose up
```

## Helm-Based Installation in Kubernetes
Requires Helm (Tested on `3.1.2`)
```
helm install alpha-and-beta microservices
```
Test by port-forwarding (Replace pod name with one you have):
```
sudo kubectl port-forward alpha-and-beta-696775d959-frqlj 80:5000
curl -X POST --data '{"message": "12341234"}'  -H "Content-Type: application/json" http://localhost:80/api/
```
