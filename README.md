# Microservices

See individual readmes for each sub-project:
- [Alpha](alpha/README.md)
- [Beta](beta/README.md)
- [Integration Tests](tests/README.md)
- [Deployment Scripts](deployment/README.md)

See [solution and deployment strategy](docs/system-arch.md).
See [CI/CD and downtime strategy](docs/ci-cd.md).

## Usage

### Quick Local Test
Requires `docker-compose` (tested on version `1.25.4`) and Docker (tested on `19.03.1`)

Spin up the services:
```
make compose
```

Test the service manually:
```
curl -X POST --data '{"message": "12341234"}'  -H "Content-Type: application/json" http://localhost:5000/api/
```

Run the integration tests (Requires Poetry and Python to be installed):
```
make integration
```

### Helm-based Installation in Kubernetes
1. Go to the [deployment](deployment) folder.
2. Run `helm install alpha-and-beta microservices`
3. This should create a `deployment` and a `service`
4. You can easily test it by port forwarding:
```
sudo kubectl port-forward alpha-and-beta-696775d959-frqlj 80:5000
curl -X POST --data '{"message": "12341234"}'  -H "Content-Type: application/json" http://localhost:80/api/
```
