import os

HOST = os.environ.get("HOST", "127.0.0.1")
BETA_ENDPOINT = os.environ.get("BETA_ENDPOINT", "http://127.0.0.1/")
