import requests
from random import randint

import settings


def reverse_text(text):
    response = requests.post(url=settings.BETA_ENDPOINT, json={"message": text})
    return response.json()["message"]


def generate_random():
    return randint(0, 100000)
