from unittest import TestCase
from src.__main__ import app


class TestApp(TestCase):
    @classmethod
    def setUp(cls):
        cls.client = app.test_client()

    def test_index(self):
        resp = self.client.get("/")
        self.assertEqual(200, resp.status_code)
        self.assertEqual(b"It works", resp.data)
