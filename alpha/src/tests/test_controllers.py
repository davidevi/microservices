from unittest import TestCase, mock
from controllers import generate_random, reverse_text


def mock_post_request(url, json):
    class MockResponse(object):
        def json(self):
            return {"message": "4321"}

    return MockResponse()


class TestControllers(TestCase):
    def test_generate_random(self):
        a = generate_random()
        self.assertIsInstance(a, int)

    @mock.patch("controllers.requests.post", mock_post_request)
    def test_reverse_text(self):
        result = reverse_text("1234")
        self.assertEqual("4321", result)
