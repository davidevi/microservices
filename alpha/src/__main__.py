from flask import Flask

from routes import api
import settings

app = Flask(__name__)
app.register_blueprint(api)


@app.route("/")
def index():
    return "It works"


if __name__ == "__main__":
    app.run(host=settings.HOST)
