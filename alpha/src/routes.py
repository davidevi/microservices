from flask import Blueprint, request, jsonify
from controllers import generate_random, reverse_text

api = Blueprint("api", __name__, url_prefix="/api", template_folder="templates")


@api.route("/", methods=["POST"])
def index():
    try:
        message = request.json["message"]
    except TypeError as e:
        return {"error": "Expected JSON data" + str(e)}, 400
    except KeyError as e:
        return {"error": "'message' is required" + str(e)}, 400

    try:
        reversed_text = reverse_text(message)
    except Exception as e:
        return {"error": "Service currently not available " + str(e)}, 503

    return jsonify({"message": reversed_text, "rand": generate_random()})
