# Beta Microservice

## Set Up
Requires the following on your machine:
- Python 3.7+
- Poetry

Install Python dependencies using Poetry:
```
poetry install
```

### Environment Variables
- `HOST` (Default: `127.0.0.1`) - Sets what host to accept requests from. Set to `0.0.0.0` to allow requests from everywhere.
- `PORT` (Default: `5000`) - Sets what port to serve the service on.

## Usage
Run all commands conveniently by starting off with `poetry shell`; Alternatively, run all commands prefixed by `poetry run ...`.

**Method 1:**
```
poetry shell
invoke run
```
**Method 2:**
```
poetry run invoke run
```
### Run in development mode
```
invoke run
```
### Run unit tests
```
invoke test
```
### Format code
```
invoke format
```
### Lint code
```
invoke lint
```
### Create docker image
```
invoke docker
```
