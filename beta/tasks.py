from invoke import task

SRC_DIR = "src"

DOCKER_REPO = "registry.gitlab.com/davidevi/microservices/"
DOCKER_IMAGE_NAME = "beta"


@task
def run(context):
    """
        Runs the microservice in development mode
    """
    with context.cd(SRC_DIR):
        context.run("python __main__.py")


@task
def test(context):
    """
        Runs the unit tests
    """
    with context.cd(SRC_DIR):
        context.run(
            "pytest --cov=. --cov-branch tests", pty=True, env={"PYTHONPATH": "../"}
        )


@task
def lint(context):
    """
        Lints with flake8
    """
    context.run(f"flake8 {SRC_DIR}")


@task
def format(context):
    """
        Auto-formats code with black
    """
    context.run(f"black {SRC_DIR}")


@task
def docker(context):
    """
        Creates docker image for microservice
    """
    context.run("pip3 freeze > requirements.txt")

    DOCKER_IMAGE_TAG = context.run("git describe --tags", hide="both").stdout.strip()

    # Adding '/' at the end of repo name in case user forgot
    separator = ""
    if len(DOCKER_REPO) > 0:
        if DOCKER_REPO[len(DOCKER_REPO) - 1] != "/":
            separator = "/"
            print("Please ensure DOCKER_REPO constant ends with a '/'")

    DOCKER_IMAGE_FULL = (
        f"{DOCKER_REPO}{separator}{DOCKER_IMAGE_NAME}:{DOCKER_IMAGE_TAG}"
    )
    context.run(f"docker build --tag {DOCKER_IMAGE_FULL} .")
    context.run(f"docker push {DOCKER_IMAGE_FULL}")
