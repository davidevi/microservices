from unittest import TestCase
from controllers import reverse_text


class TestControllers(TestCase):
    def test_reverse_text(self):
        self.assertEqual("4321", reverse_text("1234"))
        self.assertEqual("4343 2121", reverse_text("1212 3434"))
        self.assertEqual("qwerQWER1234!@#$", reverse_text("$#@!4321REWQrewq"))
