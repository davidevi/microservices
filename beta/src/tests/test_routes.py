from unittest import TestCase, mock
from src.__main__ import app


def mock_reverse_text(text):
    return "4321"


class TestRoutes(TestCase):
    @classmethod
    def setUp(cls):
        cls.client = app.test_client()

    @mock.patch("routes.reverse_text", mock_reverse_text)
    def test_endpoint_serves(self):
        """
            Endpoint must return 200 and correct result when
            the input data is valid
        """
        resp = self.client.post("/reverse", json={"message": "1234"})
        self.assertEqual(200, resp.status_code)
        response_json = resp.get_json()

        self.assertIn("message", response_json)
        self.assertEqual("4321", response_json["message"])

    def test_endpoint_bad_format(self):
        """
            Must return 400 if required parameter is not provided
        """
        resp = self.client.post("/reverse", json={"msg": "1234"})
        self.assertEqual(400, resp.status_code)

    def test_endpoint_bad_content(self):
        """
            Must return 400 if data is not provided as JSON
        """
        resp = self.client.post("/reverse")
        self.assertEqual(400, resp.status_code)
