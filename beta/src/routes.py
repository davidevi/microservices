from flask import Blueprint, request, jsonify
from controllers import reverse_text

api = Blueprint("api", __name__, template_folder="templates")


@api.route("/reverse", methods=["POST"])
def index():
    try:
        message = request.get_json()["message"]
    except TypeError:
        return {"error": "Expected JSON data"}, 400
    except KeyError:
        return {"error": "'message' is required"}, 400

    return jsonify({"message": reverse_text(message)})
