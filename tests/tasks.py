from invoke import task


@task
def integration(context):
    context.run("pytest .", pty=True)
