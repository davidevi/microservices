# Microservices Integration Tests


## Set Up
Requires the following on your machine:
- Python 3.7+
- Poetry

Install Python dependencies using Poetry:
```
poetry install
```

## Usage
Run all commands conveniently by starting off with `poetry shell`; Alternatively, run all commands prefixed by `poetry run ...`.

**Method 1:**
```
poetry shell
invoke integration
```
**Method 2:**
```
poetry run invoke integration
```
### Run integration tests
```
poetry run invoke integration
```
