from unittest import TestCase

import requests

ALPHA_ENDPOINT = "http://localhost:5000/api/"


class TestIntegration(TestCase):
    def test_reverse(self):
        resp = requests.post(url=ALPHA_ENDPOINT, json={"message": "1234"})

        self.assertEqual(200, resp.status_code)

        response_json = resp.json()

        self.assertIn("message", response_json)
        self.assertIn("rand", response_json)
        self.assertEqual("4321", response_json["message"])
        self.assertIsInstance(response_json["rand"], int)
